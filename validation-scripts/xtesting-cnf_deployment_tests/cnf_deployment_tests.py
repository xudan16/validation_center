#!/usr/bin/env python

# pylint: disable=missing-docstring

import time
import sys
import os
from termcolor import colored
from colorama import Fore, Style
from prettytable import PrettyTable
from kubernetes import client, config
from xtesting.core import testcase

# Driver Class for checking Pods deployment
class Deployment(testcase.TestCase):

    def run(self, **kwargs):
        try:
            self.start_time = time.time()
            config.load_kube_config()
            kubectl = client.CoreV1Api()
            namespaces = list(os.environ.get('NAMESPACES').split(","))
            summary_table = PrettyTable(field_names=["Pod name", "Namespace", "Phase", "Ready"])

            for n in namespaces:
                # Get pods information and store it in a list
                pods = [
                    (
                        p.metadata.name,
                        p.metadata.namespace,
                        p.status.phase,

                        # Are all its containers really ok?
                        any(c.type=="Ready" and c.status=="True" for c in p.status.conditions),
                    )
                    for p in kubectl.list_namespaced_pod(namespace=n).items
                ]

                # Metadata of unhealthy pods
                unhealthy_pods = [
                    p for p in pods
                    if (
                       (p[2] not in ("Running", "Completed", "Succeeded"))
                       or
                       (p[2]=="Running" and not p[3])
                    )
                ]

                # Counters
                pods_counter = len(pods)
                unhealthy_pods_counter = len(unhealthy_pods)
                healthy_pods_counter = pods_counter - unhealthy_pods_counter

                print(Fore.YELLOW + '\nCounts for namespace "' + n + '":' '\nPods = ', pods_counter,
                    '\nHealthy pods =', healthy_pods_counter,'\nUnhealthy pods =', str(unhealthy_pods_counter) + Style.RESET_ALL)

                if unhealthy_pods_counter != 0:
                    for i in unhealthy_pods:
                        summary_table.add_row(i)

            if summary_table.rows:
                print(Fore.RED + '\nThe following pods are UNHEALTHY:\n' + Style.RESET_ALL)
                print(colored(summary_table, 'red'), '\n')
                self.result = 0
            else:
                print(Fore.GREEN + '\nAll the pods are HEALTHY.\n' + Style.RESET_ALL)
                self.result = 100
            self.stop_time = time.time()
            
        except Exception: # pylint: disable=broad-except
            print("Unexpected error:", sys.exc_info()[0])
            self.result = 0
            self.stop_time = time.time()
