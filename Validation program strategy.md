# Scope
The Validation program aims to verify the use of the Open Source released. 

This verification is split in two independent processes

-  1 validation of CNFs, which is the confirmation that CNFs can be deployed on the stack, making use of the capabilities integrated (e.g.: DPDK, SR-IOV, ...)  
 
-  2 validation of derivative stacks, which is the confirmation that any distribution created out of the Open Source released also incorporrates the capabilities expected



## CNF validation
The Validation of a given CNF or a derived stack must be done for each version released of SYLVA stack (v1, v1.1, v1.2, ...) and is composed of the following steps


The process of Validation of CNFs is independent per CNF (per functionality) but incorporates a set of common steps for all CNFs.

This process requires an environment where to test that a CNF can successfully run on top of the stack, therefore it is required to have, at least, one "validation platform" deployed with the Open Source code released (and corresponding version). It is possible to have more than one "validation platform" available for different reasons, for example in order to validate CNFs on different versions, to specialise an environment for specific tests or to run validations with different hardware

The steps to follow in the validation process of a CNF are the following ones:

 - Identify what capabilities of the stack are required by the CNF under validation (not all CNFs require the same capabilities, for instance not all require SR-IOV support)

 - Enable in the "validation platform" the capabilities required by the CNF under test. This step should be automated since it is common for many CNFs requiring the same capabilities
 
 - Confirm that the capabilities required by the CNF under test are enabled in the "validation platform". This step should be automated since it is common for many CNFs requiring the same capabilities

 - Deploy the CNF on top of the "validation platform". This step must be performed by the CNF provider since it may require specific configuration and/or use of sensitive data from the provider
 
 - Confirm that the CNF has been deployed (e.g.: checking pods are in ready state). This step should be automated since it is common for many CNFs requiring the same capabilities
 
 - Setup a minimal configuration of the deployed CNF. This step must be defined by the CNF provider and automated as much as possible to be able to rerun it for validation over different versions of the stack
 
 - Define a minimum functional test to confirm the CNF is exploiting the capabilities from the stack. This step must be defined by the CNF provider. **THE CNF VALIDATION IS INTENDED TO CONFIRM THE CNF CAN BE DEPLOYED AND IT IS FUNCTIONAL BUT IT DOES NOT REPLACE A COMPLETE FUNCTIONAL/PERFORMANCE TEST FOR THE CNF.**
 
 - Run the minimum functional test to confirm the CNF is expplouting the capabilities from the stack. This step must be automated as much as possible to be able to rerun it for validation over different versions of the stack
 
 - Additionally complete the CNCF test suite (https://github.com/cncf/cnf-testsuite/) to verify their design is cloud native


## Derivative stack validation
The process of Validating a derived stacks is requires a set of "reference previously validated CNFs" to demonstrate that they can run on top of teh derived stack same as they did when validated on top of the "validation platform".  

This process requires a "hardware environment" where to test that the derived stack can be deployed and CNFs can successfully make use of the required capabilities. It is possible to have more than one "hardware environment" available for different reasons, for example in order to validate the derived stackcan onboard CNFs with different hardware underneath

The steps to follow in the validation process of a derived stack will be based on a set of test procedures to confirm the different Telco capabilities integrated in the Open Source project are actually present in the derived stack under test and can be successfully exercised by the "reference previously validated CNFs"  (e.g.: to confirm datarate is guaranteed generate traffic, inject and measure the actual response, to confirm a plug-in such as multus can be enabled execute the process to configure it and confirm it can be exercised by a CNF).
